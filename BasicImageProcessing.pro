QT       += core gui widgets

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

include($$PWD/CustomWidget/CustomWidget.pri)
include($$PWD/Config/Config.pri)
include($$PWD/BasicOperations/BasicOperations.pro)


TARGET = BasicImageProcessing
TEMPLATE = app

DEFINES += QT_DEPRECATED_WARNINGS

SOURCES += main.cpp \
    mainwindow.cpp \
    testw.cpp


HEADERS  += mainwindow.h \
    testw.h


FORMS    += mainwindow.ui \
    testw.ui


INCLUDEPATH += C:\OutSources\OpenCV310\include \

CONFIG(release,debug|release)
{
     LIBS +=         C:\OutSources\OpenCV310\lib\vc14\x64\Release\*.lib
}

CONFIG(debug,debug|release)
{
     LIBS +=         C:\OutSources\OpenCV310\lib\vc14\x64\Debug\*.lib

}

