#ifndef CONFIG_H
#define CONFIG_H

enum methods{

       CANNY,
       SOBEL,
       LAPLACIAN,
       EROSION,
       DILATION,
       OPEN,
       CLOSE,
       THINING,
       THIKENING,
       GUSSIAN,
       MEDIAN,
       MEAN,
       HISTOGRAMEQ,
       LOGTRANSFER,
       NEGATIVE,
       ROTATE,
       RESIZE,
       FLIP,
       DISTORTION

   };

#endif // CONFIG_H
