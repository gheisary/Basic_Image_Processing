#ifndef MAINWINDOW_H
#define MAINWINDOW_H
#include "maintree.h"
#include "mainsetting.h"
#include "Config/config.h"
#include <QLabel>
#include <QMainWindow>
#include <QFileDialog>
#include <opencv2/core.hpp>
#include "BasicOperations/Edge/canny.h"
#include "BasicOperations/Morphological/erosion.h"
#include "BasicOperations/Edge/canny.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    QImage      mainImage;
    cv::Mat     mainImageMat;
    QLabel      *lblMainImage;

private:
    Ui::MainWindow *ui;
    MainTree *mainTree;
    MainSetting *mainSetting;
    Canny *canny;
    QImage mainImage;
    Erosion *er;



public slots:
    void test(int i);
    void treeSignalReciever(QTreeWidgetItem*  ,int);
    void cannyExecuteReciever(int);
    cv::Mat     QImage2Mat(QImage const& src);
    QImage      Mat2QImage(cv::Mat const& src);
    void drowImage();
    void on_actionOpen_triggered();
};

#endif // MAINWINDOW_H
