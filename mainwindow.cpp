#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDebug>
#include <QVBoxLayout>
#include <QPushButton>

#include "testw.h"
#include "BasicOperations/Edge/canny.h"


MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent), ui(new Ui::MainWindow)
{

    ui->setupUi(this);

    lblMainImage    = new QLabel;
    lblMainImage->setAlignment(Qt::AlignCenter);
    setCentralWidget(lblMainImage);

    mainTree = new MainTree(this);
    this->addDockWidget(Qt::LeftDockWidgetArea,mainTree->doc);

    mainSetting = new MainSetting;
    this->addDockWidget(Qt::LeftDockWidgetArea,mainSetting->doc);

    connect(mainTree,SIGNAL(sendIndex(int)),this,SLOT(test(int)));



}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::test(int i)
{
    qDebug()  << QString::number(i);
}



void MainWindow::treeSignalReciever(QTreeWidgetItem* treeItem ,int index)
{

    QLayoutItem *child;
    while ((child = mainSetting->mainVLayout->takeAt(0)) != 0) {
        delete child->widget();
        delete child;
    }

    switch (treeItem->text(1).toInt()) {
    case CANNY:
       {

        canny = new Canny;
        mainSetting->mainVLayout->addWidget(canny);

        connect(canny,SIGNAL(cannyEcecute(int)),this,SLOT(cannyExecuteReciever(int)));

        break;
       }
    case SOBEL:
    {
        testw *tw = new testw;
        mainSetting->mainVLayout->addWidget(tw);

        break;
    }
    case EROSION:
    {
        er = new Erosion;
        mainSetting->mainVLayout->addWidget(er);
        connect(er,SIGNAL(run()),this,SLOT(drowImage()));

        break;
    }
    default:

        qDebug()  << treeItem->text(1);
        break;
    }

}

void MainWindow::cannyExecuteReciever(int threshold)
{
    QImage image = lblMainImage->pixmap()->toImage();
        if(image.isNull())
            return;

//   QImage edges = canny->execute(image);

   cv::Mat edgesMat = canny->execute(QImage2Mat(mainImage), threshold);

   QImage edges = Mat2QImage(edgesMat);

   lblMainImage->setPixmap(QPixmap::fromImage(edges));
}

void MainWindow::drowImage()
{
    er->execute(&mainImageMat);
    mainImage = Mat2QImage(mainImageMat);
    lblMainImage->setPixmap(QPixmap::fromImage(mainImage));
}

void MainWindow::on_actionOpen_triggered()
{
    QString fname = QFileDialog::getOpenFileName(this, "Open", qApp->applicationDirPath(), "Images (*.jpg *.bmp *.tif *.png)");
       if(!QFile(fname).exists())
           return;

       mainImage = QImage(fname);
       mainImageMat = QImage2Mat(mainImage);

       mainImage = image;

       mainImageMat = QImage2Mat(image);

       lblMainImage->setPixmap(QPixmap::fromImage(image));
}

QImage MainWindow::Mat2QImage(cv::Mat const& src)
{
    QImage dest(src.cols, src.rows, QImage::Format_ARGB32);

      const float scale = 255.0;

      if (src.depth() == CV_8U) {
        if (src.channels() == 1) {
          for (int i = 0; i < src.rows; ++i) {
            for (int j = 0; j < src.cols; ++j) {
              int level = src.at<quint8>(i, j);
              dest.setPixel(j, i, qRgb(level, level, level));
            }
          }
        } else if (src.channels() == 3) {
          for (int i = 0; i < src.rows; ++i) {
            for (int j = 0; j < src.cols; ++j) {
              cv::Vec3b bgr = src.at<cv::Vec3b>(i, j);
              dest.setPixel(j, i, qRgb(bgr[2], bgr[1], bgr[0]));
            }
          }
        }
      } else if (src.depth() == CV_32F) {
        if (src.channels() == 1) {
          for (int i = 0; i < src.rows; ++i) {
            for (int j = 0; j < src.cols; ++j) {
              int level = scale * src.at<float>(i, j);
              dest.setPixel(j, i, qRgb(level, level, level));
            }
          }
        } else if (src.channels() == 3) {
          for (int i = 0; i < src.rows; ++i) {
            for (int j = 0; j < src.cols; ++j) {
              cv::Vec3f bgr = scale * src.at<cv::Vec3f>(i, j);
              dest.setPixel(j, i, qRgb(bgr[2], bgr[1], bgr[0]));
            }
          }
        }
      }

      return dest;
}


cv::Mat MainWindow::QImage2Mat(QImage const& src)

{
    cv::Mat mat = cv::Mat(src.height(), src.width(), CV_8UC4, (uchar*)src.bits(), src.bytesPerLine());
    cv::Mat mat2 = cv::Mat(mat.rows, mat.cols, CV_8UC3 );
    int from_to[] = { 0,0, 1,1, 2,2 };
    cv::mixChannels( &mat, 1, &mat2, 1, from_to, 3 );

    return mat2;
}

