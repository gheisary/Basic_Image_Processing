FORMS += \
    $$PWD/canny.ui \
    $$PWD/sobel.ui

HEADERS += \
    $$PWD/canny.h \
    $$PWD/sobel.h

SOURCES += \
    $$PWD/canny.cpp \
    $$PWD/sobel.cpp
