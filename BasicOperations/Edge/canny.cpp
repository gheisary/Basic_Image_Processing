#include "canny.h"
#include "ui_canny.h"

Canny::Canny(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Canny)
{
    ui->setupUi(this);


}

Canny::~Canny()
{
    delete ui;
}



QImage Canny::execute(QImage image)
{
    QImage edges = image;

    QMatrix3x3 kernel;
    kernel(0, 0) = -1; kernel(0, 1) = -1; kernel(0, 2) = -1;
    kernel(1, 0) = -1; kernel(1, 1) = 8; kernel(1, 2) = -1;
    kernel(2, 0) = -1; kernel(2, 1) = -1; kernel(2, 2) = -1;

    for(int i=1; i<image.width()-1; i++)
    {
        for(int j=1; j<image.height()-1; j++)
        {
            image.setPixel(i, j, qGray(image.pixel(i, j)));
        }
    }

    for(int i=1; i<image.width()-1; i++)
    {
        for(int j=1; j<image.height()-1; j++)
        {
            float v = 0;

            // *****************************************************
            v =
                    kernel(0, 0) * qGray(image.pixel(i+1, j+1)) +
                    kernel(0, 1) * qGray(image.pixel(i, j+1)) +
                    kernel(0, 2) * qGray(image.pixel(i-1, j+1)) +

                    kernel(1, 0) * qGray(image.pixel(i+1, j)) +
                    kernel(1, 1) * qGray(image.pixel(i, j)) +
                    kernel(1, 2) * qGray(image.pixel(i-1, j)) +

                    kernel(2, 0) * qGray(image.pixel(i+1, j-1)) +
                    kernel(2, 1) * qGray(image.pixel(i, j-1)) +
                    kernel(2, 2) * qGray(image.pixel(i-1, j-1));

            edges.setPixel(i,j, qRgb(v, v, v));

        }
    }

    return edges;
}

cv::Mat Canny::execute(cv::Mat matImage, int threshold)
{
    cv::Mat tmp = matImage;
    cv::Mat src = matImage;

    cv::cvtColor(tmp,src,CV_RGB2GRAY);

    cv::Canny(src,tmp,50,threshold,3);

    return tmp;
}

void Canny::on_btnExecute_clicked()
{
}

void Canny::on_horizontalSlider_sliderMoved(int position)
{

    emit cannyEcecute(position);
}
