#ifndef EROSION_H
#define EROSION_H

#include <QWidget>
#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>

namespace Ui {
class Erosion;
}

class Erosion : public QWidget
{
    Q_OBJECT

public:
    Erosion(QWidget *parent = 0);
    ~Erosion();
    void execute(cv::Mat*);

public slots:

    void on_btnRun_clicked();

signals:
    void run();

private:
    Ui::Erosion *ui;
    int erode_type;
    int erode_size;
};

#endif // EROSION_H
