#ifndef MAINTREE_H
#define MAINTREE_H
#include <QtCore>
#include <QtGui>
#include <QTreeWidget>
#include <QVBoxLayout>
#include <QGroupBox>
#include <QDockWidget>
#include "qmainwindow.h"

class MainTree : public QWidget
{
Q_OBJECT
public:
    MainTree(QMainWindow *);
    QDockWidget *doc;
    QTreeWidget *tree;
    void init();
    void addRoot(QTreeWidgetItem *item, QString name);
    void addChild(QTreeWidgetItem *parent, QString name, int code);

signals:
    void sendIndex(int);
public slots:
};

#endif // MAINTREE_H
