#include "maintree.h"
#include "testw.h"
#include "mainwindow.h"

MainTree::MainTree(QMainWindow *qMainWindow):
    QWidget()
{
    tree = new QTreeWidget;

    init();

    doc = new QDockWidget;

    QVBoxLayout *mainVLayout = new QVBoxLayout();
    QGroupBox *mainGroupBox = new QGroupBox();

    mainGroupBox->setLayout(mainVLayout);

    doc->setWidget(mainGroupBox);
    mainVLayout->addWidget(tree);

    connect(tree,SIGNAL(itemClicked(QTreeWidgetItem*,int)), qMainWindow, SLOT(treeSignalReciever(QTreeWidgetItem*,int)));
}


void MainTree::init()
{
    tree->setColumnCount(2);
    tree->hideColumn(1);
    tree->setHeaderLabel("Functions");


    QTreeWidgetItem *item = new QTreeWidgetItem;
    addRoot(item ,"first");
    addChild(item,"subF1",8);
    addChild(item,"subF2",9);

    QTreeWidgetItem *item3 = new QTreeWidgetItem;
    addRoot(item3 ,"Morphology");
    addChild(item3,"Erosion",EROSION);

    QTreeWidgetItem *item2 = new QTreeWidgetItem;
    addRoot(item2 ,"edge");
    addChild(item2,"canny",CANNY);
    addChild(item2,"sobel",SOBEL);
}

void MainTree::addRoot(QTreeWidgetItem *item, QString name)
{
    item->setText(0,name);
    item->setText(1,"-1");
    item->setTextAlignment(0,1);
    tree->addTopLevelItem(item);
}

void MainTree::addChild(QTreeWidgetItem *parent, QString name, int code)
{
    QTreeWidgetItem *item = new QTreeWidgetItem();
    item->setText(0,name);
    item->setText(1,QString::number(code));
    item->setTextAlignment(0,1);
    parent->addChild(item);
}
